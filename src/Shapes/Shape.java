
package Shapes;


abstract class Shape {

public Shape(){}

abstract double getArea(double d);
abstract double getPerimeter(double d);
}
