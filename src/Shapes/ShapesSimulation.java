package Shapes;


public class ShapesSimulation {

    public static void main(String[] args) {
      
        //make circle
        Circle c1 = new Circle();
        
        //Print out circles area + perimeter
        System.out.println("Circle area = " + c1.getArea(8));
        System.out.println("Circle Perimeter = " + c1.getPerimeter(8));
        System.out.println("--------------------------");
        
        //make square
        Square s1 = new Square();
        //print out squares area + perimeter
        System.out.println("Square area = " + s1.getArea(6));
        System.out.println("Square Perimeter = " + s1.getPerimeter(6));
        System.out.println("--------------------------");
        
    
    }
    
}
