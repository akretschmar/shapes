
package Shapes;


public class Circle extends Shape{
    
    
public Circle(){}

@Override
public double getArea(double d){
    return Math.PI * d*d;
}

public double getPerimeter(double d){
    return 2 * Math.PI * d;
}
    
    
}
