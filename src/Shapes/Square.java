
package Shapes;


public class Square extends Shape{

    
public Square(){}

@Override
public double getArea(double d){
    return d * d;
}

public double getPerimeter(double d){
    return 4 * d;
}
}
